# Biodisc-Viz


## Abstract

Machine learning (ML) algorithms are powerful tools able to find complex patterns  and biomarker signatures where conventional statistical methods and humans fail to identify them But the ML field is sophisticated, and state of the art methodologies to build efficient and non-overfitting models are not always applied in the litterature. To this purpose, automatic programs, such as BioDiscML, have been designed to identify biomarker signatures and its correlated features while escaping overfitting using various evaluation strategies. But this tool lacks visualization support and the decision of choosing the best models and signatures is not straightforward. Thus, with the optic to give the researcher an easily accessible and usable tool to study in depth the results from BiodiscML outputs, we developed a visual interaction tool called BiodiscViz.  

BiodiscViz provides summary, table and graphics, in the form of a PCAs, heatmap and boxplots for the best model and the correlated features. Furthermore, this tool also provides visual support to extract a consensus signature from BioDiscML models using a combination of filters.


## Short Description

Biodisc-Viz is a vizualisation tool for BiodiscML outputs. Biodisc-Viz is a Rshiny application for studying biomarkers and signatures through machine learning models.

## Web application

There is a web application of the app which can be found on [shinyapp.io](https://sophiane-bouirdene.shinyapps.io/BiodiscViz_shinyapp/)

## Requirements

- Install R
[windows](https://cran.r-project.org/bin/windows/base/)
linux:
```bash
sudo apt install r-base
```
- Install [Rstudio](https://www.rstudio.com/products/rstudio/download/)
- Install Rshiny
```r
install.packages("shiny")
```
- Install [Rtools](https://cran.r-project.org/bin/windows/Rtools/rtools43/rtools.html)

## Installation

To install Biodisc-Viz you just need to clone the git page.

```bash
git clone https://gitlab.com/SBouirdene/biodiscml_output_app.git
```

## Usage

### Web

To start the application you need to click on the <https://sophiane-bouirdene.shinyapps.io/BiodiscViz_shinyapp/>  which will redirect you to online version of the app.

Once on the application you have to click on <span style="color:red">browse</span> and select all the files in the directory containing biodiscML outputs then <span style="color:green">Submit</span>.
![Search Directory](Images/tutorial_select_search_directory_web.png)

### Local

To start the application you need to click on the <span style="color:red">run button</span> on Rstudio
![Run BiodiscViz](Images/tutorial_run_biodiscviz.png)


Once on the application you have to click on <span style="color:red">input directory</span> and select the directory containing biodiscML outputs then <span style="color:green">Submit</span>.
![Search Directory](Images/tutorial_select_search_directory_new.png)

### Run example 

If you want to run the example without looking for it in the file just click on <span style="color:purple">Example</span> 


### Visualisation of the results

The differents sections will appear.

**Short and Long signature** are the results from the best model found by BiodiscML. These <span style="color:red">two sections</span>. are divided in <span style="color:green">three part</span>  which are a summary, plots and table. The summary as its name implies is a description of the results, the plots contains three plots : a heatmap, a PCA plot, Umap, t-SNE plot and a bloxplot, which font and label size can be changed with the <span style="color:purple">sliders</span> reactively. Finally the table section is a way to look into the csv at the origin of the plots. You can switch between each section just by clicking on it. 
![Short and long signature](Images/tutorial_shortsignature_new.png)

**Attribute distribution** shows the distribution of the most called signatures by the differents classifiers tested with BiodiscML. It also gives the possibility for the user to add quality threshold (<span style="color:red">Mathew Correlation Coefficient</span>   and <span style="color:green"> Standard Deviation </span> ) for the classifiers kept in the call of the signatures. There is also a dynamic threshold for <span style="color:purple">the number of signature</span> displayed.
![Consensus threshold selection](Images/tutorial_consensus_threshold_selection_new.png)

**Consensus Signature** presents the distribution of the previously selected signatures across three plots which are : a PCA , a Heatmap  Umap, t-SNE and a boxplot.
![Consensus signatures](Images/tutorial_consensus_signature_new.png)

### Interpretation of Boxplots
- ns: p > 0.05

- *: p <= 0.05

- **: p <= 0.01

- ***: p <= 0.001

- ****: p <= 0.0001


## Support
sophiane.bouirdene@crchudequebec.ulaval.ca

## Updates

None



## Authors and acknowledgment

Sophiane Bouirdene, Arnaud Droit Laboratory, Centre Hospitalier de l'Université Laval (developer)  
Mickaël Leclercq,  Arnaud Droit Laboratory, Centre Hospitalier de l'Université Laval (developer)  



Léopold Quitté,  Arnaud Droit Laboratory, Centre Hospitalier de l'Université Laval (tester)  
Elloyse Coyle, Arnaud Droit Laboratory, Centre Hospitalier de l'Université Laval (tester)  




Arnaud Droit,  Arnaud Droit Laboratory, Centre Hospitalier de l'Université Laval (director)  
Steve Bilodeau,   Centre Hospitalier de l'Université Laval (director)  


## License

Biodisc-Viz is available under the GPL-3.0 license. See the LICENSE file and spdx ids in source code for more info.
